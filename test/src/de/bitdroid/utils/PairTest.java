package de.bitdroid.utils;

import junit.framework.TestCase;


public final class PairTest extends TestCase {
	
	public void testHashAndEquals() {
		Pair<String,String> pair1 = new Pair<String,String>("hello", "world");
		Pair<String,String> pair2 = new Pair<String,String>("hello", "world");
		Pair<String,String> pair3 = new Pair<String,String>("hello", null);

		assertTrue("equals failed", pair1.equals(pair2));
		assertTrue("equals failed", !pair1.equals(pair3));
		assertTrue("equals failed", !pair1.equals(null));

		assertTrue("hash failed", pair1.hashCode() == pair2.hashCode());
		assertTrue("hash failed", pair1.hashCode() != pair3.hashCode());
	}
}
