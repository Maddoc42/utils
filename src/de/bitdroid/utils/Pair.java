package de.bitdroid.utils;


public final class Pair<T1,T2> {

	private final T1 value1;
	private final T2 value2;
	
	public Pair(T1 value1, T2 value2) {
		this.value1 = value1;
		this.value2 = value2;
	}

	public T1 getElement1() { return value1; }
	public T2 getElement2() { return value2; }

	@Override
	@SuppressWarnings("rawtypes")
	public boolean equals(Object other) {
		if (other == null || !(other instanceof Pair)) return false;
		Pair pair = (Pair) other;
		return Pair.equals(value1, pair.value1) && Pair.equals(value2, pair.value2);
	}

	@Override
	public int hashCode() {
		final int MULT = 17;
		int hash = 13;
		hash = hash * MULT + ((value1 == null) ? 0 : value1.hashCode());
		hash = hash * MULT + ((value2 == null) ? 0 : value2.hashCode());
		return hash;
	}

	private static <T> boolean equals(T t1, T t2) {
		if (t1 == null && t2 == null) return true;
		if (t1 == null) return false;
		if (t2 == null) return false;
		return t1.equals(t2);
	}
}
